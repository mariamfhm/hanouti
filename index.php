<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hanouti</title>
    <link rel="stylesheet" href="css/style.css?v=<?php echo time(); ?>">
</head>
<body>
    <header>
        <div>
            <b>Hanouti</b>
        </div>
    </header>
    <div id="container">
        <div id="products">
            <?php
                include "data.php";
                foreach($prod as $p){
                    echo "<div class='box'>";
                    echo "<img src='imgs/$p[img]'>";
                    echo "<h3>$p[name] : $p[price] DH</h3>";
                    echo "<button>Detail</button>";
                    echo "<a href='/TP06_GI/index.php?id=$p[id]' class='add'>add</a>";
                    echo "</div>";
            }
            ?>
        </div>
        <div id="cart">
            <?php
                include 'cart.php';
            ?>
        </div>
    </div>
   
</body>
</html>